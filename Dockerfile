# Sử dụng hình ảnh PHP-FPM chính thức
FROM php:8.0-fpm

# Sao chép nội dung từ thư mục public vào thư mục HTML
COPY public/ /var/www/html

# Cài đặt thêm các phần mở rộng PHP nếu cần
RUN docker-php-ext-install mysqli
